Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: "users/registrations" }
  resources :users, only: [:show, :index]
  get 'icon_slot', to: "users#show_icon_modal"
  post 'change_slot', to: "users#change_slot"

  match 'users/:id' => 'users#destroy', :via => :delete, :as => :admin_destroy_user
  get 'rankings', to: 'users#rankings'

  resources :articles
  get 'tsviewer', to: 'articles#tsViewer'

  get 'store', to: "items#store"
  get 'inventory', to: 'items#inventory'
  post 'buy', to: "items#buy"
  post 'use', to: "items#use"
  get 'store_item', to: "items#store_item_modal"
  get 'inventory_item', to: "items#inventory_item_modal"
  
  root 'articles#index'
  get 'wip', to: 'welcome#index'
end