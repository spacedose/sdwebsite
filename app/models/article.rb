class Article < ApplicationRecord

    has_one_attached :cover
    has_rich_text :content

    validates :title,
        presence: true
    
    validates :author,
        presence: true

end
