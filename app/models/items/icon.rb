class Icon < Item

    def use (user)
        super
    end

    def get_flash_success
        return "Icon Unlocked!"
    end

    def get_flash_error
        super
    end    
end