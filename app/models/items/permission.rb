class Permission < Item

    def use (user)
        begin
            ts = Teamspeak::Client.new("spacedose.de", "10011")
            ts.login('serveradmin', 'gFlQ9VUE')
            ts.command('use', sid: 1)
            id = ts.command('clientdbfind', {"pattern" => user.ts_identity}, '-uid').first["cldbid"]
            ts.command('servergroupaddclient', {"sgid" => self.value, "cldbid" => id})
            ts.command('logout')
            ts.disconnect        
            super
        rescue StandardError => e
            puts e
            return false
        end
    end

    def get_flash_success
        return "Server Group Assigned!"
    end

    def get_flash_error
        return "TeamSpeak Server Error - Group Already Applied?"
    end    
end