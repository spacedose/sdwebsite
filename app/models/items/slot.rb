class Slot < Item

    def use (user)
        super
    end

    def get_flash_success
        return "Slot Successfully Added!"
    end

    def get_flash_error
        super
    end    
end