class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :rememberable, :validatable

  has_many :inventories
  has_many :items, through: :inventories

  TS_IDENTITY_FORMAT = /\A
      (?=\A.{27}[=]) # Must contain 27 characters and end with '='
  /x

  has_one_attached :avatar
  
  validates :avatar, 
    content_type: [:png, :jpg, :jpeg], 
    size: { less_than: 256.kilobytes , message: 'is too big. Maximum size should be 256KB.' }

  validates :username, 
    presence: true, 
    uniqueness: true,
    :length => { :maximum => 14 }

  validates :ts_identity, 
    presence: true, 
    uniqueness: true,
    format: { with: TS_IDENTITY_FORMAT }

  def email_required?
    false
  end

  def will_save_change_to_email?
    false
  end

  def update_icons
    @icons = Item.where(type: "Icon").pluck(:value)
    @currentIcons = Inventory.joins("INNER JOIN items ON inventories.item_id = items.id and inventories.user_id = #{self.id} and inventories.used = 1 and items.type = 'Slot'")

    begin
      ts = Teamspeak::Client.new("spacedose.de", "10011")
      ts.login('serveradmin', 'gFlQ9VUE')
      ts.command('use', sid: 1)
      id = ts.command('clientdbfind', {"pattern" => self.ts_identity}, '-uid').first["cldbid"]

      @groups = ts.command('servergroupsbyclientid', {"cldbid" => id})

      @groups.each do |t|
        if @icons.include?(t["sgid"]) then
          ts.command('servergroupdelclient', {"sgid" => t["sgid"], "cldbid" => id})
        end
      end
      
      @currentIcons.each do |t|
        if t.value != nil then
          ts.command('servergroupaddclient', {"sgid" => t.value, "cldbid" => id})
        end
      end

      ts.command('logout')
      ts.disconnect
      
      return true
    rescue StandardError => e
      puts e
      return false
    end
  end

end
