class Item < ApplicationRecord

    require 'teamspeak-ruby'

    has_many :inventories
    has_many :users, through: :inventories

    def use (user)
        if (self.unique) then
            user.inventories.where(item_id: self.id, used: 0).first.update(used: 1)
        else
            user.inventories.where(item_id: self.id, used: 0).first.delete
        end

        return true
    end

    def get_flash_success
        return "Item used successfully"
    end

    def get_flash_error
        return "Something went wrong :("
    end        
end