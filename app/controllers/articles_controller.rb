class ArticlesController < ApplicationController
  
  before_action :is_admin?, only: [:new, :edit, :update, :destroy, :create]

  include Pagy::Backend

  def new
    @article = Article.new
  end

  def show
    @article = Article.find(params[:id])
    @author = User.find(@article.author).username
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])

    if @article.update(article_params)
      redirect_to @article
    else
      render 'edit'
    end
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy
   
    redirect_to articles_path
  end

  def index
    @pagy, @articles = pagy(Article.all.reverse_order, items: 5)
  end

  def create
    @article = Article.new(article_params)
    @article.author = current_user.id
    @article.save
    redirect_to root_path
  end

  private

  def article_params
    params.require(:article).permit(:title, :content, :author, :cover)
  end

end