class ApplicationController < ActionController::Base

    before_action :configure_permitted_parameters, if: :devise_controller?

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :ts_identity, :password, :password_confirmation])
      devise_parameter_sanitizer.permit(:account_update, keys: [:avatar, :private])
    end

    def asset_exists?(path)
        if Rails.env.production?  
          Rails.application.assets_manifest.find_sources(path) != nil
        else
          Rails.application.assets.find_asset(path) != nil
        end
    end

    helper_method :asset_exists?

    private

    def is_admin?
      if (user_signed_in?)
        redirect_to root_path unless current_user.admin
      else
        redirect_to root_path
      end  
    end

end
