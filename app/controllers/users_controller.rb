class UsersController < ApplicationController
    before_action :is_admin?, only: [:index, :destroy]
    before_action :authenticate_user!

    def index
        @users = User.all
    end

    def show
        @user = User.find(params[:id])
        
        # Icons
        @slots = Inventory.joins("INNER JOIN items ON inventories.item_id = items.id and inventories.user_id = #{current_user.id} and inventories.used = 1 and items.type = 'Slot'")
        @iconNames = []

        @slots.each do |s|
            if s.value != nil then
                @iconNames.push(Item.where(type: "Icon").where(value: s.value).first.name)
            else
                @iconNames.push(Item.find(s.item_id).name)
            end
        end

        # Trophies
        @nothing = true
        # POI_total
        @poi_total = User.where(private: false).order("POI_total DESC").limit(3)
        # POI
        @poi = User.where(private: false).order("POI DESC").limit(3)
    end

    def show_icon_modal
        @unlockedIcons = Item.joins("INNER JOIN inventories ON inventories.item_id = items.id and inventories.user_id = #{current_user.id}").where("inventories.used = 1").where(type: "Icon")
        @slots = Item.joins("INNER JOIN inventories ON inventories.item_id = items.id and inventories.user_id = #{current_user.id}").where("inventories.used = 1").where(type: "Slot")

        respond_to do |format|
            format.js {render 'show_icon_modal'}
        end
    end

    def change_slot

        @slots = Inventory.joins("INNER JOIN items ON inventories.item_id = items.id and inventories.user_id = #{current_user.id} and inventories.used = 1 and items.type = 'Slot'")
        
        @slots.each_with_index do |slot, index|
            if params[:new_icon_id] != nil && params[:new_icon_id].length > index && params[:new_icon_id][index] != nil then
                slot.value = params[:new_icon_id][index]
            else
                slot.value = nil
            end
            slot.save
        end
        
        current_user.update_icons
        redirect_to user_path(current_user.id)
    end

    def rankings
        @poi_total = User.where(private: false).order("POI_total DESC")
        @poi = User.where(private: false).order("POI DESC")
    end

    def destroy
        @user = User.find(params[:id])
        @user.destroy
    
        if @user.destroy
            redirect_to users_path, notice: "User deleted."
        end
    end
  
end