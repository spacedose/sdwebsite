class ItemsController < ApplicationController
    before_action :authenticate_user!

    def index
    end

    def store
        @exclude = Item.joins("INNER JOIN inventories ON inventories.item_id = items.id and inventories.user_id = #{current_user.id}").where(unique: 1).ids
        @items = Item.where.not(id: @exclude)
        @alreadyBought = Item.where(id: @exclude)
    end

    def inventory
        @items = Item.joins("INNER JOIN inventories ON inventories.item_id = items.id and inventories.user_id = #{current_user.id}").where("inventories.used = 0")
        #@itemsUsed = Item.joins("INNER JOIN inventories ON inventories.item_id = items.id").where("inventories.used = 1", unique: 1)
    end

    def buy
        @item = Item.find(params[:item_id])

        if @item.price > current_user.POI then
            redirect_to store_path, notice: "You don't have enough POI :("
        else
            current_user.POI -= @item.price
            current_user.save
            current_user.items << @item
            flash[:success] = "Purchase Successful - Paid #{@item.price} POI."
            redirect_to store_path
        end 
    end

    def store_item_modal
        @item = Item.find_by_id(params[:item_id])

        respond_to do |format|
            format.js {render 'store_item_modal'}
        end
    end
    
    def inventory_item_modal
        @item = Item.find_by_id(params[:item_id])

        respond_to do |format|
            format.js {render 'inventory_item_modal'}
        end
    end

    def use
        @item = current_user.items.find(params[:item_id])

        if ( @item.use(current_user) ) then
            flash[:success] = @item.get_flash_success
        else
            flash[:error] = @item.get_flash_error
        end
        
        redirect_to inventory_path
    end
end