var id = "1086593"
var text_color = "C8C8C8"
var text_size = "12"
var text_family = "6"

var text_s_color = "C8C8C8" 
var text_s_weight = "normal"
var text_s_style = "normal"
var text_s_variant = "normal"
var text_s_decoration = "none"

var text_i_color = ""
var text_i_weight = "normal"
var text_i_style = "normal"
var text_i_variant = "normal"
var text_i_decoration = "none"

var text_c_color = ""
var text_c_weight = "normal"
var text_c_style = "normal"
var text_c_variant = "normal"
var text_c_decoration = "none"

var text_u_color = "C8C8C8"
var text_u_weight = "normal"
var text_u_style = "normal"
var text_u_variant = "normal"
var text_u_decoration = "none"

var text_s_color_h = ""
var text_s_weight_h = "bold"
var text_s_style_h = "normal"
var text_s_variant_h = "normal"
var text_s_decoration_h = "none"

var text_i_color_h = "C8C8C8"
var text_i_weight_h = "bold"
var text_i_style_h = "normal"
var text_i_variant_h = "normal"
var text_i_decoration_h = "none"

var text_c_color_h = ""
var text_c_weight_h = "normal"
var text_c_style_h = "normal"
var text_c_variant_h = "normal"
var text_c_decoration_h = "none"

var text_u_color_h = ""
var text_u_weight_h = "bold"
var text_u_style_h = "normal"
var text_u_variant_h = "normal"
var text_u_decoration_h = "none"

var iconset = "default"

var ts3v_url_1 = "https://www.tsviewer.com/ts3viewer.php?" 
    + "ID=" + id 
    + "&text=" + text_color 
    + "&text_size=" + text_size 
    + "&text_family=" + text_family 
    + "&text_s_color=" + text_s_color 
    + "&text_s_weight=" + text_s_weight 
    + "&text_s_style=" + text_s_style 
    + "&text_s_variant=" + text_s_variant 
    + "&text_s_decoration=" + text_s_decoration 
    + "&text_i_color=" + text_i_color 
    + "&text_i_weight=" + text_i_weight 
    + "&text_i_style=" + text_i_style 
    + "&text_i_variant=" + text_i_variant 
    + "&text_i_decoration=" + text_i_decoration 
    + "&text_c_color=" + text_c_color 
    + "&text_c_weight=" + text_c_weight 
    + "&text_c_style=" + text_c_style 
    + "&text_c_variant=" + text_c_variant 
    + "&text_c_decoration=" + text_c_decoration 
    + "&text_u_color=" + text_u_color 
    + "&text_u_weight=" + text_u_weight 
    + "&text_u_style=" + text_u_style 
    + "&text_u_variant=" + text_u_variant 
    + "&text_u_decoration=" + text_u_decoration 
    + "&text_s_color_h=" + text_s_color_h 
    + "&text_s_weight_h=" + text_s_weight_h 
    + "&text_s_style_h=" + text_s_style_h 
    + "&text_s_variant_h=" + text_s_variant_h 
    + "&text_s_decoration_h=" + text_s_decoration_h 
    + "&text_i_color_h=" + text_i_color_h 
    + "&text_i_weight_h=" + text_i_weight_h 
    + "&text_i_style_h=" + text_i_style_h 
    + "&text_i_variant_h=" + text_i_variant_h 
    + "&text_i_decoration_h=" + text_i_decoration_h 
    + "&text_c_color_h=" + text_c_color_h 
    + "&text_c_weight_h=" + text_c_weight_h 
    + "&text_c_style_h=" + text_c_style_h 
    + "&text_c_variant_h=" + text_c_variant_h 
    + "&text_c_decoration_h=" + text_c_decoration_h 
    + "&text_u_color_h=" + text_u_color_h 
    + "&text_u_weight_h=" + text_u_weight_h 
    + "&text_u_style_h=" + text_u_style_h 
    + "&text_u_variant_h=" + text_u_variant_h 
    + "&text_u_decoration_h=" + text_u_decoration_h 
    + "&iconset=" + iconset;

ts3v_display.init(ts3v_url_1, 1086593, 100);