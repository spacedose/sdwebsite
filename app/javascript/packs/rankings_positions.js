$(document).ready(function(){

    // fix flickering
    swap()
    $('.trophies').css("visibility", "visible")

    $(window).on("resize", function (e) {
        swap()
    })

    function swap () {
        
        $('.trophies').each(function() {
            
            if ($(document).width() > 991) {
                if (this.children[0].children[0].classList.contains("place-0")) {
                    this.children[0].before(this.children[1])
                }
            } else {
                if (this.children[0].children[0].classList.contains("place-1")) {
                    this.children[0].before(this.children[1])
                }
            }
        })
    }

    $('.popover').remove()
})
