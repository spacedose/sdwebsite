$(document).on('turbolinks:load', function() {

    var resolutionX = 1920
    var resolutionY = 1280
    var currentScale = window.innerWidth / resolutionX

    var animationCanvas = {
        canvas : document.createElement("canvas"),
        start : function() {
            this.canvas.width = window.innerWidth;
            this.canvas.height = window.innerWidth * resolutionY / resolutionX;
            this.context = this.canvas.getContext("2d");
            document.body.insertBefore(this.canvas, document.body.childNodes[0]);
            this.frameNo = 0;
            this.interval = setInterval(updateGameArea, 40);
        },
        stop : function() {
            clearInterval(this.interval);
        },    
        clear : function() {
            this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        },
        resize : function () {
            currentScale = window.innerWidth / resolutionX
            this.canvas.width = window.innerWidth;
            this.canvas.height = window.innerWidth * resolutionY / resolutionX;
        }
    }
    
    function circle(radius, color, x, y, type) {
    
        this.type = type;
        this.radius = radius
        this.speed = 0.1;
        this.angle = 0;
        this.moveAngle = 15;
        this.originalX = x
        this.originalY = y
        this.x = x;
        this.y = y;    
        this.update = function() {
            ctx = animationCanvas.context;
            ctx.save();
            ctx.translate(this.x * currentScale, this.y * currentScale);
            ctx.fillStyle = color;
            ctx.beginPath()
            ctx.arc(0, 0, radius * currentScale, 0, 2 * Math.PI)
            ctx.fill()
            ctx.restore();    
        }
        this.newPos = function() {
                    
            var distance = Math.sqrt(Math.pow((this.x - this.originalX), 2) + Math.pow((this.y - this.originalY), 2))

            var lengthA = Math.abs(this.originalX - this.x)
            var lengthB = Math.abs(this.originalY - this.y)
            var beta = Math.atan(lengthB / lengthA) * 180 / Math.PI

            var actualAngle = mod(Math.round(this.angle * 180 / Math.PI), 360)
            actualAngle = mod(actualAngle + 90, 360)

            var relativeAngle
            if (this.x > this.originalX) {
                // right
                if (this.y > this.originalY) {
                    // bottom
                    relativeAngle = mod(actualAngle - beta + 180, 360)
                } else {
                    // top
                    relativeAngle = mod(actualAngle + beta + 180, 360)
                }
            } else {
                // left
                if (this.y > this.originalY) {
                    // bottom
                    relativeAngle = mod(actualAngle + beta, 360)
                } else {
                    // top
                    relativeAngle = mod(actualAngle - beta, 360)
                }
            }

            if (distance > 25) {
                if (relativeAngle < 180) {
                    this.angle += Math.floor(Math.random() * 15) * Math.PI / 180
                } else {
                    this.angle -= Math.floor(Math.random() * 15) * Math.PI / 180
                }
            } else {
                this.angle += (Math.floor(Math.random() * 30) - 15) * Math.PI / 180
            }  
                 
            this.x += this.speed * Math.sin(this.angle);
            this.y -= this.speed * Math.cos(this.angle);
        }
    }

    function line (circleA, circleB, color, size, type) {
        this.type = type;
        this.color = color;
        this.size = size;
        this.circleA = circleA
        this.circleB = circleB
        this.newPos = function() {
        }
        this.update = function() {
            ctx = animationCanvas.context
            ctx.strokeStyle = color
            ctx.lineWidth = size * animationCanvas.canvas.width / 1920
            ctx.beginPath()
            ctx.moveTo(this.circleA.x * currentScale, this.circleA.y * currentScale)
            ctx.lineTo(this.circleB.x * currentScale, this.circleB.y * currentScale)
            ctx.stroke()
        }
    }

    function text (text, size, color, x, y, type) {
        this.type = type
        this.text = text
        this.size = size
        this.color = color
        this.x = x
        this.y = y
        this.newPos = function() {

        }
        this.update = function() {
            ctx = animationCanvas.context
            ctx.fillStyle = this.color
            ctx.textAlign = "end"
            ctx.font = 'bold ' + (this.size * currentScale|0) + "px Ubuntu";
            ctx.fillText(this.text, this.x * currentScale, this.y * currentScale);
        }
    }
    
    function updateGameArea() {
        
        animationCanvas.clear();
        animationCanvas.resize();
        circlesBackground.forEach(update)
        linesBackground.forEach(update)
        circlesForeground.forEach(update)
        linesForeground.forEach(update)
        title.update()
        subtitle.update()
    }

    function update(c) {
        c.newPos();
        c.update();
    }

    // Initiliaze Pattern
    function startGame() {
        
        title = new text("SPACEDOSE.DE", 110, "#ffeb3f", 1700, 475)
        subtitle = new text("work in progress.", 35, "#ffeb3f", 1700, 525)

        circlesForeground = []
        circlesBackground = []
        linesForeground = []
        linesBackground = []
        
        // background
        circlesBackground.push(new circle(17, "#6a621b", 260, -50))   // Pos:  0
        circlesBackground.push(new circle(17, "#6a621b", 985, -50))   // Pos:  1
        circlesBackground.push(new circle(17, "#6a621b", 1175, -50))  // Pos:  2
        circlesBackground.push(new circle(17, "#6a621b", 1500, -50))  // Pos:  3
        circlesBackground.push(new circle(17, "#6a621b", 1580, -50))  // Pos:  4
        circlesBackground.push(new circle(17, "#6a621b", 1765, -50))  // Pos:  5
        circlesBackground.push(new circle(17, "#6a621b", 1950, -50))  // Pos:  6
        circlesBackground.push(new circle(17, "#6a621b", 360, 0))     // Pos:  7
        circlesBackground.push(new circle(17, "#6a621b", 930, 45))    // Pos:  8
        circlesBackground.push(new circle(17, "#6a621b", 1150, 45))   // Pos:  9
        circlesBackground.push(new circle(17, "#6a621b", 1570, 85))   // Pos: 10
        circlesBackground.push(new circle(17, "#6a621b", 1880, 65))   // Pos: 11
        circlesBackground.push(new circle(17, "#6a621b", 655, 75))    // Pos: 13
        circlesBackground.push(new circle(17, "#6a621b", 280, 100))   // Pos: 14
        circlesBackground.push(new circle(17, "#6a621b", 5, 100))     // Pos: 15
        circlesBackground.push(new circle(17, "#6a621b", 2025, 125))  // Pos: 12
        circlesBackground.push(new circle(17, "#6a621b", 1250, 155))  // Pos: 22
        circlesBackground.push(new circle(17, "#6a621b", 525, 175))   // Pos: 16
        circlesBackground.push(new circle(17, "#6a621b", 205, 240))   // Pos: 17
        circlesBackground.push(new circle(17, "#6a621b", 1700, 235))  // Pos: 22
        circlesBackground.push(new circle(17, "#6a621b", 1470, 255))  // Pos: 22
        circlesBackground.push(new circle(17, "#6a621b", 442, 355))   // Pos: 18
        circlesBackground.push(new circle(17, "#6a621b", 400, 460))   // Pos: 19
        circlesBackground.push(new circle(17, "#6a621b", -50, 435))   // Pos: 20
        circlesBackground.push(new circle(17, "#6a621b", 110, 540))   // Pos: 21
        circlesBackground.push(new circle(17, "#6a621b", 675, 600))   // Pos: 22
        circlesBackground.push(new circle(17, "#6a621b", 310, 665))   // Pos: 22
        circlesBackground.push(new circle(17, "#6a621b", 630, 745))   // Pos: 22
        circlesBackground.push(new circle(17, "#6a621b", 375, 830))   // Pos: 22
        circlesBackground.push(new circle(17, "#6a621b", 70, 825))    // Pos: 22
        circlesBackground.push(new circle(17, "#6a621b", -50, 820))   // Pos: 22
        
        radius = 6
        linesBackground.push(new line(circlesBackground[3], circlesBackground[10], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[4], circlesBackground[10], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[5], circlesBackground[10], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[6], circlesBackground[11], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[10], circlesBackground[16], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[10], circlesBackground[19], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[10], circlesBackground[20], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[2], circlesBackground[9], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[9], circlesBackground[16], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[1], circlesBackground[8], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[8], circlesBackground[12], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[11], circlesBackground[15], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[11], circlesBackground[19], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[0], circlesBackground[13], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[13], circlesBackground[14], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[13], circlesBackground[17], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[13], circlesBackground[18], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[17], circlesBackground[21], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[18], circlesBackground[21], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[21], circlesBackground[22], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[21], circlesBackground[24], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[21], circlesBackground[25], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[23], circlesBackground[24], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[24], circlesBackground[26], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[24], circlesBackground[29], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[29], circlesBackground[30], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[25], circlesBackground[27], "#6a621b", radius))
        linesBackground.push(new line(circlesBackground[27], circlesBackground[28], "#6a621b", radius))
        
        // foreground
        circlesForeground.push(new circle(17, "#ffeb3f", 750, -115))   // Pos:  0
        circlesForeground.push(new circle(17, "#ffeb3f", 290, 75))     // Pos:  1
        circlesForeground.push(new circle(30, "#ffeb3f", 270, 250))    // Pos:  2
        circlesForeground.push(new circle(17, "#ffeb3f", 670, 240))    // Pos:  3
        circlesForeground.push(new circle(30, "#ffeb3f", 645, 360))    // Pos:  4
        circlesForeground.push(new circle(17, "#ffeb3f", 735, 375))    // Pos:  5
        circlesForeground.push(new circle(13, "#ffeb3f", -80, 385))    // Pos:  6
        circlesForeground.push(new circle(60, "#ffeb3f", 530, 540))    // Pos:  7
        circlesForeground.push(new circle(17, "#ffeb3f", 230, 555))    // Pos:  8
        circlesForeground.push(new circle(30, "#ffeb3f", 400, 605))    // Pos:  9
        circlesForeground.push(new circle(13, "#ffeb3f", 810, 635))    // Pos: 10
        circlesForeground.push(new circle(25, "#ffeb3f", 670, 695))    // Pos: 11
        circlesForeground.push(new circle(13, "#ffeb3f", 845, 715))    // Pos: 12
        circlesForeground.push(new circle(35, "#ffeb3f", 180, 815))    // Pos: 13
        circlesForeground.push(new circle(17, "#ffeb3f", 705, 815))    // Pos: 14
        circlesForeground.push(new circle(30, "#ffeb3f", 975, 835))    // Pos: 15
        circlesForeground.push(new circle(17, "#ffeb3f", -80, 935))    // Pos: 16
        circlesForeground.push(new circle(20, "#ffeb3f", 305, 940))    // Pos: 17
        circlesForeground.push(new circle(17, "#ffeb3f", 565, 950))    // Pos: 18
        circlesForeground.push(new circle(17, "#ffeb3f", 1355, 965))   // Pos: 19
        circlesForeground.push(new circle(17, "#ffeb3f", 1085, 1015))  // Pos: 20
        circlesForeground.push(new circle(13, "#ffeb3f", 175, 1045))   // Pos: 21
        circlesForeground.push(new circle(20, "#ffeb3f", 760, 1065))   // Pos: 22
        circlesForeground.push(new circle(17, "#ffeb3f", 495, 1140))   // Pos: 23
        
        radius = 8
        linesForeground.push(new line(circlesForeground[0], circlesForeground[2], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[1], circlesForeground[2], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[2], circlesForeground[4], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[2], circlesForeground[6], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[3], circlesForeground[4], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[4], circlesForeground[5], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[4], circlesForeground[7], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[7], circlesForeground[9], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[7], circlesForeground[11], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[7], circlesForeground[18], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[8], circlesForeground[9], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[9], circlesForeground[13], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[10], circlesForeground[11], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[11], circlesForeground[12], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[13], circlesForeground[16], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[13], circlesForeground[17], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[13], circlesForeground[21], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[14], circlesForeground[18], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[15], circlesForeground[18], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[15], circlesForeground[19], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[15], circlesForeground[20], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[18], circlesForeground[22], "#ffeb3f", radius))
        linesForeground.push(new line(circlesForeground[18], circlesForeground[23], "#ffeb3f", radius))
           
        animationCanvas.start();
    }

    startGame()

    var opacity = 1;
    var fade = setInterval(dimmer, 20)
    function dimmer () {
        if (opacity <= 0) clearInterval(fade) 
        opacity -= 0.01
        document.getElementById("fade").style.opacity = opacity
    }
})

// mod from libary doesn't work with negative numbers
function mod(n, m) {
    return ((n % m) + m) % m;
}