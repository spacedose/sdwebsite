$(document).on('turbolinks:load', function() {
    
    /* Buggy, some color flickering on reload
    $('.brand-icon').addClass(function () {
        var colors = ["orange", "green", "yellow"]
        return colors[Math.floor(Math.random() * colors.length)]
    })
    */
    
    // Navbar mobile support
    $(window).on("resize", function (e) {

        if ($(window).width() < 992) {
            if ($('.btn-profile-dropdown').attr("aria-expanded") === "true") {
                $('.collapse').collapse('show')
                $('.navbar-hide').hide()
            }
        } else {
            $('.navbar-hide').show()
        }
    })

    $('.btn-profile-dropdown').click(function () {
        if ($('.navbar-toggler').is(":visible")) {
            $('.navbar-hide').toggle()
        } 
    })

    $('.navbar-collapse').on("show.bs.collapse", function () {
        $('.navbar-hide').show()
    })
    
    $(function () {
        $('[data-toggle="popover"]').popover()
    })

})