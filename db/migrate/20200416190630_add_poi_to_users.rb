class AddPoiToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :POI, :integer, default: 0
    add_column :users, :POI_total, :integer, default: 0
  end
end
