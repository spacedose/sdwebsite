class CreateItemsOfUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :inventories do |t|
      t.belongs_to :user
      t.belongs_to :item
      t.boolean :used, null: false, default: false

      t.timestamps
    end
  end
end
