class AddValueToInventories < ActiveRecord::Migration[6.0]
  def change
    add_column :inventories, :value, :integer
  end
end
