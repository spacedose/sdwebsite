class CreateItem < ActiveRecord::Migration[6.0]
  def change
    create_table :items do |t|
      t.string :name
      t.text :description
      t.integer :price
      t.boolean :unique
      t.string :type

      t.timestamps
    end
  end
end