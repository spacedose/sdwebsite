# frozen_string_literal: true

class DeviseCreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      ## Database authenticatable
      t.string :username,           null: false, default: ""
      t.string :ts_identity,        null: false, default: ""
      t.string :encrypted_password, null: false, default: ""
      t.boolean :admin,             null: false, default: false

      ## Rememberable
      t.datetime :remember_created_at

      t.timestamps null: false
    end

    add_index :users, :username,             unique: true
    add_index :users, :ts_identity,          unique: true

  end
end
